import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'auth.login',
        component: () => import(/* webpackChunkName: "login" */ '../views/auth/Login.vue')
    },
    {
        path: '/posts',
        name: 'posts.index',
        component: () => import(/* webpackChunkName: "posts index" */ '../views/posts/Index.vue')
    },
    {
        path: '/posts/subjects/:subjectSlug',
        name: 'subjects.show',
        component: () => import(/* webpackChunkName: "subjects show" */ '../views/subjects/Show.vue')
    },
    {
        path: '/posts/:subjectSlug/:postSlug',
        name: 'posts.show',
        component: () => import(/* webpackChunkName: "posts show" */ '../views/posts/Show.vue')
    },
    {
        path: '/posts/new',
        name: 'posts.new',
        component: () => import(/* webpackChunkName: "new post" */ '../views/posts/New.vue'),
        meta: {
            auth: true,
        }
    },
    {
        path: '/posts/:subjectSlug/:postSlug/edit',
        name: 'posts.edit',
        component: () => import(/* webpackChunkName: "posts edit" */ '../views/posts/Edit.vue'),
        meta: {
            auth: true,
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'active',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.meta.auth && !store.getters['auth/check']) next('/login')
    else next()

    if (to.name == 'auth.login' && store.getters['auth/check']) next({ name: 'home' })
    else next()
})

export default router
