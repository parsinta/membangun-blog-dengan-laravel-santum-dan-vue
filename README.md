# Membangun Blog dengan Laravel Santum dan Vue

### The Project Api
Take the source from [here](https://bitbucket.org/parsinta/api-membangun-blog-dengan-laravel-santum-dan-vue).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
